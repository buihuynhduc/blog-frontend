import {createRouter, createWebHistory, RouteRecordRaw} from 'vue-router';
import authMiddleware from "../../middleware/authMiddleware";
import HomeView from '../views/HomeView.vue';
import store from "@/store";
import notFoundMiddleware from "../../middleware/notFoundMiddleware";

const routes: Array<RouteRecordRaw> = [
    {
        path: '/', name: 'home', component: HomeView, meta: {middleware: [authMiddleware]} // Sử dụng middleware ở đây
    },
    {
        path: '/about',
        name: 'about',
        meta: {middleware: [authMiddleware]},
        component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
    },
    {
        path: '/login', name: 'login', component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
    },
    {
        path: '/register',
        name: 'register',
        component: () => import(/* webpackChunkName: "about" */ '../views/Register.vue')
    },
    {
        path: '/404', name: '404', component: () => import('@/views/404View.vue')
    }];

const router = createRouter({
    history: createWebHistory(), routes
});

router.beforeEach((to, from, next) => {
    const middleware = to.meta.middleware;
    if (middleware) {
        const cookies = (window as any).$cookies
        // @ts-ignore
        middleware.forEach((mw) => {
            mw(store, next, cookies);
        });
    }
    next();
});

notFoundMiddleware({router});

export default router;
