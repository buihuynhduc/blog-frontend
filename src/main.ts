import {createApp} from 'vue'
import App from './App.vue'
import axios from "axios";
import router from './router'
import store from "@/store";
import VueCookies from 'vue-cookies';
import Echo from 'laravel-echo';
import {io} from "socket.io-client";

const app = createApp(App).use(store).use(router).use(VueCookies);
axios.defaults.baseURL = process.env.VUE_APP_API_BASE_URL
app.config.globalProperties.$store = store;
app.config.globalProperties.$http = axios;


// Cấu hình tiêu đề CORS cho Axios
axios.defaults.headers.common['Access-Control-Allow-Origin'] = 'http://localhost:8080';
axios.defaults.headers.common['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS';
axios.defaults.headers.common['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, Content-Type, Accept';
axios.defaults.headers.common['Access-Control-Allow-Credentials'] = 'true';

(window as any).io = io;
(window as any).Echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001',
    auth: {
        headers: {
            Authorization: null,
        },
    },
});

app.mount('#app');