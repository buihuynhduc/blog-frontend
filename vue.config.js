const Dotenv = require('dotenv-webpack');

module.exports = {
  transpileDependencies: ['vue'],
  configureWebpack: {
    plugins: [
      new Dotenv()
    ]
  },
};
