export default function (store : any, next : any){
    if (!localStorage.getItem('token')) {
        return next('/login')
    }
}