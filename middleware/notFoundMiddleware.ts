import { Router, RouteRecordRaw } from 'vue-router';

export default function ({router}: { router: Router }) {
    router.beforeEach((to, from, next) => {
        const issetEndPoint:RouteRecordRaw | undefined = router.getRoutes().find((route) => {
            return route.path === to.path;
        });

        if (!issetEndPoint) {
            next('/404');
        } else {
            next();
        }
    });
}