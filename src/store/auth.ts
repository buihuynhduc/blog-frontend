import { Module, MutationTree, ActionTree, GetterTree } from 'vuex';

interface AuthState {
  token: String | null
}

const state: AuthState = {
  token: null
}

const mutations: MutationTree<AuthState> = {
  setToken(state, token) {
    state.token = token;
  },
  clearToken(state) {
    state.token = null;
  },
};

const actions: ActionTree<AuthState, any> = {
  login({ commit }, token: string) {
    commit('setToken', token);
  },
  logout({ commit }) {
    commit('clearToken');
  },
};

const getters: GetterTree<AuthState, any> = {
  getToken(state) {
    return state.token;
  },
};

const auth: Module<AuthState, any> = {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};

export default auth;