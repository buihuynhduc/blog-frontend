import { reactive, toRefs } from '@vue/composition-api';

// Tạo một reactive object để lưu trữ dữ liệu
const state = reactive({
    token: localStorage.getItem('token') || null,
});

// Hàm setter để cập nhật giá trị của token và lưu vào localStorage
function setToken(token : string) {
    state.token = token;
    localStorage.setItem('token', token);
}

// Hàm getter để truy cập giá trị của token
function getToken() {
    return state.token;
}

// Export reactive state và getter, setter qua toRefs
export default {
    ...toRefs(state),
    setToken,
    getToken,
};